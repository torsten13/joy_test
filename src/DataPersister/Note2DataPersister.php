<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Note2;

final class Note2DataPersister implements ContextAwareDataPersisterInterface
{

    public function supports($data, array $context = []): bool
        {
            return $data instanceof Note2;
        }

        public function persist($data, array $context = [])
        {
          // call your persistence layer to save $data
          // TODO Hier nutzen wir wieder rdbms
          // $data->setHeadline('super');
          return $data;
        }

        public function remove($data, array $context = [])
        {
          // call your persistence layer to delete $data
          // TODO Auch hier rdbms rein
        }
}