<?php

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\Exception\ResourceClassNotSupportedException;
use App\Entity\Testclass;

final class TestclassDataProvider implements ItemDataProviderInterface, CollectionDataProviderInterface, RestrictedDataProviderInterface
{
    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Testclass::class === $resourceClass;
    }

    public function getCollection(string $resourceClass, string $operationName = null): \Generator
    {
        // Hier rufen wir dann load_by_id oder so was auf.
        yield new Testclass(1);
        yield new Testclass(2);
    }

    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?Testclass
    {
        // Retrieve the blog post item from somewhere then return it or null if not found
        return new Testclass($id);
    }
}