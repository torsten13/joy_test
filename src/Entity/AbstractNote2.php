<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * AbstractNote2
 *
 */
class AbstractNote2
{
    /**
     * @var int
     *
     */
    public $id;

    /**
     * @var int
     *
     */
    public $userId;

    /**
     * @var string
     *
     * @Assert\NotBlank
     */
    public $headline;

    /**
     * @var string
     *
     */
    public $content;

    /**
     * @var int|null
     *
     */
    public $targetTime;

    /**
     * @var int|null
     *
     */
    public $remindTime;

    /**
     * @var bool
     *
     */
    public $toRemind = '0';

    /**
     * @var int|null
     *
     */
    public $relatedSupportCaseId;



    function __construct($id) {
         $this->id = $id;
    }

    public function getId(): ?int
        {
            return $this->id;
        }

        public function getUserId(): ?int
        {
            return $this->userId;
        }

        public function setUserId(int $userId): self
        {
            $this->userId = $userId;

            return $this;
        }

        public function getHeadline(): ?string
        {
            return $this->headline;
        }

        public function setHeadline(string $headline): self
        {
            $this->headline = $headline;

            return $this;
        }

        public function getContent(): ?string
        {
            return $this->content;
        }

        public function setContent(string $content): self
        {
            $this->content = $content;

            return $this;
        }

        public function getTargetTime(): ?int
        {
            return $this->targetTime;
        }

        public function setTargetTime(?int $targetTime): self
        {
            $this->targetTime = $targetTime;

            return $this;
        }

        public function getRemindTime(): ?int
        {
            return $this->remindTime;
        }

        public function setRemindTime(?int $remindTime): self
        {
            $this->remindTime = $remindTime;

            return $this;
        }

        public function getToRemind(): ?bool
        {
            return $this->toRemind;
        }

        public function setToRemind(bool $toRemind): self
        {
            $this->toRemind = $toRemind;

            return $this;
        }

        public function getRelatedSupportCaseId(): ?int
        {
            return $this->relatedSupportCaseId;
        }

        public function setRelatedSupportCaseId(?int $relatedSupportCaseId): self
        {
            $this->relatedSupportCaseId = $relatedSupportCaseId;

            return $this;
        }
}
