<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * Note
 *
 * @ApiResource()
 *
 * @ORM\Table(name="note")
 * @ORM\Entity
 */
class Note
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="headline", type="string", length=40, nullable=false, options={"fixed"=true})
     */
    private $headline;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=65535, nullable=false)
     */
    private $content;

    /**
     * @var int|null
     *
     * @ORM\Column(name="target_time", type="integer", nullable=true, options={"unsigned"=true,"comment"="Termin für die Notiz (optional)"})
     */
    private $targetTime;

    /**
     * @var int|null
     *
     * @ORM\Column(name="remind_time", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $remindTime;

    /**
     * @var bool
     *
     * @ORM\Column(name="to_remind", type="boolean", nullable=false)
     */
    private $toRemind = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="related_support_case_id", type="integer", nullable=true, options={"unsigned"=true,"comment"="Der Supportfall für den die Erinnerung angelegt wurde"})
     */
    private $relatedSupportCaseId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getHeadline(): ?string
    {
        return $this->headline;
    }

    public function setHeadline(string $headline): self
    {
        $this->headline = $headline;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getTargetTime(): ?int
    {
        return $this->targetTime;
    }

    public function setTargetTime(?int $targetTime): self
    {
        $this->targetTime = $targetTime;

        return $this;
    }

    public function getRemindTime(): ?int
    {
        return $this->remindTime;
    }

    public function setRemindTime(?int $remindTime): self
    {
        $this->remindTime = $remindTime;

        return $this;
    }

    public function getToRemind(): ?bool
    {
        return $this->toRemind;
    }

    public function setToRemind(bool $toRemind): self
    {
        $this->toRemind = $toRemind;

        return $this;
    }

    public function getRelatedSupportCaseId(): ?int
    {
        return $this->relatedSupportCaseId;
    }

    public function setRelatedSupportCaseId(?int $relatedSupportCaseId): self
    {
        $this->relatedSupportCaseId = $relatedSupportCaseId;

        return $this;
    }


}
