<?php

namespace App\Entity;


use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;

/**
 * Testclass
 *
 * @ApiResource()
 *
 */
class Testclass {

    /**
     * @var int;
     * @ApiProperty(identifier=true)
     */
    public $id;

    /**
     * @var string;
     */
    public $word;

    function __construct($id) {
         $this->id = $id;
         $this->word = 'jaa';
    }
}